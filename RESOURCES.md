
### Tools/Resources Used

-   Prepose - for compiling scss to css (https://prepros.io/)
-   normalize.css from https://github.com/necolas/normalize.css
-   Chart.js for chart (https://www.chartjs.org/)