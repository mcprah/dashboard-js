
axios.defaults.baseURL = 'https://freddy.codesubmit.io';

//Add a response interceptor

axios.interceptors.response.use((response) => {
    return response
}, async function (error) {
    const originalRequest = error.config;

    if (error.response.status === 401 && originalRequest.url === '/refresh') {
        document.location.href = "./";
        return Promise.reject(error);
    }

    // refresh access_token
    if (error.response.status === 401 && !originalRequest._retry) {

        originalRequest._retry = true;
        const refreshToken = getCookie('r_t');


        try {
            return await axios.post('/refresh', {}, {
                headers: {
                    Authorization: 'Bearer ' + refreshToken
                }
            }).then((res) => {
                if (res.status === 200) {
                    setCookie('a_t', res.data.access_token);
                    axios.defaults.headers.common['Authorization'] = 'Bearer ' + getCookie('a_t');
                    return axios(originalRequest);
                } else {

                }
            });
        } catch (error) {
            // console.log(error)
        }
    }
    return Promise.reject(error);
});





