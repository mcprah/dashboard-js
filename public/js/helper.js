
export function showLoader(elementID, width = "40px", height = "40px") {
    document.getElementById(elementID).innerHTML = ` <div class="loader" style="width: ${width}; height: ${height};"></div>`;
}

export function showTableLoader(tableBodyId) {

    document.getElementById(tableBodyId).innerHTML = ` <tr><td colspan="4"><div class="loader"></div></td> </tr>`;
}

export function sanctifyText(rawText, toLower = true) {
    let trimmedText;
    toLower
        ? (trimmedText = rawText.trim().toLowerCase())
        : (trimmedText = rawText.trim());
    const regex = /(\r\n|\n|\r)|[ ]{2,}/gi;
    const sanctifiedText = trimmedText.replaceAll(regex, ' ');
    return sanctifiedText;
}
