import { sanctifyText, showTableLoader } from './helper.js';

// If user is logged out, redirect to login page
if (!checkCookie("a_t")) {
    document.location.href = "./";
}

// Init all orders
getOrderList({ query: ' ', page: 1 }).then((res) => {
    displayOrderList(res);
});

// If user types, wait 600ms and conduct search...preventing too many API hits whenever a letter is typed
let txtSearch = document.getElementById("txtSearch");
txtSearch.oninput = _.debounce(() => {
    let pageNo = 1;
    searchOrder(pageNo);
}, 600);

// Load searched orders
function searchOrder(page) {
    showTableLoader("ordersTBody");

    let _query = sanctifyText(txtSearch.value) ?? ' ';
    getOrderList({ query: _query, page: page }).then((res) => {
        displayOrderList(res);
    })
};

// pagination setup
function intializePaginationActions(btnPrevId, btnNextId, paginationData) {
    let btnPrev = document.getElementById(btnPrevId);
    let btnNext = document.getElementById(btnNextId);
    let pageNo;

    if (paginationData.prevPage != null) {
        btnPrev.onclick = () => {
            pageNo = paginationData.prevPage
            searchOrder(pageNo);
        }
        btnPrev.disabled = false;
    } else {
        btnPrev.disabled = true;
    }

    if (paginationData.nextPage != null) {
        btnNext.onclick = () => {
            pageNo = paginationData.nextPage
            searchOrder(pageNo);
        }
        btnNext.disabled = false;
    } else {
        btnNext.disabled = true;
    }


}

// list display
function displayOrderList(data) {

    let rowList = [];
    let orders = data.orders;
    let _currentPage = data.page;
    let trHTML;

    let pagination = {
        currentPage: _currentPage,
        nextPage: Math.round(data.total / orders.length) > 1 && _currentPage < orders.length ? _currentPage + 1 : null,
        prevPage: (_currentPage - 1) >= 1 && _currentPage <= orders.length ? _currentPage - 1 : null,
        totalResults: data.total,
    }

    if (orders.length) {
        orders.forEach(order => {
            trHTML = "<tr>";
            trHTML += "<td>" + order.product.name + "</td>";
            trHTML += "<td>" + new Date(order.created_at).toLocaleDateString() + "</td>";
            trHTML += "<td>" + order.currency + order.total.toLocaleString() + "</td>";
            trHTML += `<td class='${order.status} capitalize'>` + order.status + "</td>";
            trHTML += "</tr>";

            rowList.push(trHTML);
        });

        // display rows
        document.getElementById("ordersTBody").innerHTML = rowList.join('');
        // set pagination
        document.getElementById("currentPage").innerHTML = pagination.currentPage;
        document.getElementById("totalPages").innerHTML = Math.round(pagination.totalResults / orders.length);

        // activate pagination buttons
        intializePaginationActions("btnPrev", "btnNext", pagination);

    } else {
        showTableLoader("ordersTBody");
    }
}

// Fetch dashboard data from api
async function getOrderList({ page = 1, query }) {

    try {

        const response = await axios.get(`/orders?page=${page}&q=${query}`, {

            headers: {
                Authorization: 'Bearer ' + getCookie('a_t')
            }
        }).catch((error) => {
            return;
        })

        return response.data;
    } catch (e) {
        return;
    }
}
