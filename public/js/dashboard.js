// If user is logged out, redirect to login page
if (!checkCookie("a_t")) {
    document.location.href = "./";
}

loadDashboardData().then(async (res) => {
    let bestSellers, salesOverTimeWeek, salesOverTimeYear, dataLoading = false;
    let revenueSwitch = document.getElementById('switchInput');
    let tableBody = document.getElementById("bestSellersTBody");

    bestSellers = res.bestsellers;
    salesOverTimeWeek = res.sales_over_time_week;
    salesOverTimeYear = res.sales_over_time_year;

    showWeeklyStats(salesOverTimeWeek);
    showMonthlyStats(salesOverTimeYear);

    // show loading before chart is displayed
    if (!dataLoading) {
        document.querySelector("#chartDiagram .loader").style.visibility = "hidden";
    }
    displayRevenueChart(salesOverTimeWeek, salesOverTimeYear, revenueSwitch);
    displayBestSellersList(bestSellers, tableBody);
});

// Fetch dashboard data from api
async function loadDashboardData() {
    try {
        const response = await axios.get('/dashboard', {
            headers: {
                Authorization: 'Bearer ' + getCookie('a_t')
            }
        }).catch((error) => {
            // if (error.response.status === 401 && !originalRequest._retry) {
            //     console.log(error.response);
            // }
        })

        return response.data.dashboard;
    } catch (e) {
        return;
    }
}

function showWeeklyStats(weeklySalesData) {
    let lastWeekTotalAmount = 0,
        todayRevenue = 0,
        lastWeekOrders = 0,
        lastWeekRevenue = 0;

    /* 
        Get labels and dataset from dashboard data retrieved from API
            compute and display statistics on today's revenue and last week's;
    */

    for (var idx in weeklySalesData) {
        if (idx == "1") {
            todayRevenue = weeklySalesData[idx];
        }
        lastWeekTotalAmount += weeklySalesData[idx].total;
        lastWeekOrders += weeklySalesData[idx].orders;

    }

    lastWeekRevenue = {
        total: lastWeekTotalAmount,
        orders: lastWeekOrders
    }


    document.getElementById("todayTotalAmount").innerHTML = todayRevenue.total.toLocaleString();
    document.getElementById("todayTotalOrders").innerHTML = todayRevenue.orders.toLocaleString();
    document.getElementById("lastWeekTotalAmount").innerHTML = lastWeekRevenue.total.toLocaleString();
    document.getElementById("lastWeekTotalOrders").innerHTML = lastWeekRevenue.orders.toLocaleString();

}

function showMonthlyStats(monthlySalesData) {
    /* 
        Get labels and dataset from dashboard data retrieved from API
             compute and display statistics on last month;
    */
    for (var idx in monthlySalesData) {
        if (idx == "2") {
            lastMonthRevenue = monthlySalesData[idx];
        }

    }

    document.getElementById("lastMonthTotalAmount").innerHTML = lastMonthRevenue.total.toLocaleString();
    document.getElementById("lastMonthTotalOrders").innerHTML = lastMonthRevenue.orders.toLocaleString();

}

function displayRevenueChart(weeklySalesData, yearlyData, switchButton) {
    let weeklyChartRevenue = [],
        weeklyChartLabel = [],
        yearlyChartLabel = [],
        yearlyChartRevenue = [],
        weeklyChartData,
        yearlyChartData;

    for (var idx in weeklySalesData) {
        weeklyChartLabel.push(idx);
        weeklyChartRevenue.push(weeklySalesData[idx].total);
    }
    for (var idx in yearlyData) {
        yearlyChartLabel.push(idx);
        yearlyChartRevenue.push(yearlyData[idx].total);
    }

    weeklyChartData = {
        labels: weeklyChartLabel,
        revenue: weeklyChartRevenue,
    }
    yearlyChartData = {
        labels: yearlyChartLabel,
        revenue: yearlyChartRevenue,
    }

    // Set weekly revenue chart
    setWeeklyRevenue(weeklyChartData);

    // Toggle between weekly and yearly revenue
    switchButton.addEventListener("click", function () {
        switch (switchButton.checked) {
            case true:
                setYearlyRevenue(yearlyChartData);
                break;
            default:
                setWeeklyRevenue(weeklyChartData)
                break;
        }
    });
}

function displayBestSellersList(data, container) {
    let rowList = [], trHTML;
    // Create rows with columns to display list of bestsellers within the table body
    data.forEach(bestSeller => {
        trHTML = "<tr>";
        trHTML += "<td>" + bestSeller.product.name + "</td>";
        trHTML += "<td>" + (bestSeller.revenue / bestSeller.units).toLocaleString() + "</td>";
        trHTML += "<td>" + bestSeller.units.toLocaleString() + "</td>";
        trHTML += "<td>" + bestSeller.revenue.toLocaleString() + "</td>";
        trHTML += "</tr>";

        rowList.push(trHTML);
    });
    container.innerHTML = rowList.join('');
}

function setWeeklyRevenue(data) {
    let chartLabels, chartData;
    let chartTime = document.getElementById('chartTime');
    chartTime.innerHTML = `(last ${data.labels.length} days)`;

    // Define chart labels
    chartLabels = data.labels.map((label) => {
        switch (label) {
            case "1":
                return 'Today';
            case "2":
                return 'Yesterday';
            default:
                return `Day ${label}`;
        }
    })

    // Define chart dataset
    chartDatasets = [{
        label: 'Daily Amount',
        backgroundColor: 'rgb(103 170 1)',
        borderColor: 'rgb(103 170 1)',
        data: data.revenue,
    }];

    let chartStatus = Chart.getChart("revenueChart");

    // If chart exists, update data else create and display a new chart instance
    if (chartStatus != undefined) {
        chartStatus.data.labels = chartLabels;
        chartStatus.data.datasets = chartDatasets;
        chartStatus.update();
    } else {
        chartData = {
            labels: chartLabels,
            datasets: chartDatasets
        };

        setChartData(chartData)
    }
}

function setYearlyRevenue(data) {
    let chartLabels, chartData;
    chartTime.innerHTML = `(last ${data.labels.length} months)`;

    // Define chart labels
    chartLabels = data.labels.map((label) => {
        switch (label) {
            case "1":
                return 'This month';
            case "2":
                return 'Last month';
            default:
                return `Month ${label}`;
        }
    })

    // Define chart dataset
    chartDatasets = [{
        label: 'Montly Amount',
        backgroundColor: 'rgb(210, 92, 2)',
        borderColor: 'rgb(210, 92, 2)',
        data: data.revenue,
    }];

    let chartStatus = Chart.getChart("revenueChart");

    // If chart exists, update data else create and display a new chart instance
    if (chartStatus != undefined) {
        chartStatus.data.labels = chartLabels;
        chartStatus.data.datasets = chartDatasets;
        chartStatus.update();
    } else {
        chartData = {
            labels: chartLabels,
            datasets: chartDatasets
        };
        setChartData(chartData)
    }
}

// Data chart configuration and initialization 
function setChartData(data) {
    const config = {
        type: 'line',
        data: data,
        options: {
            legend: {
                display: false,
            }
        }
    };

    let revenueChart = new Chart(
        document.getElementById('revenueChart'),
        config
    );
}



