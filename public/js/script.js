

if (checkCookie('a_t')) {
    let c = getCookie('a_t');
    axios.defaults.headers.common['Authorization'] = `Bearer ${c}`;
}


function setCookie(name, val) {
    var date = new Date();
    date.setTime(date.getTime() + (7 * 24 * 60 * 60 * 1000));
    document.cookie = `${name}=${val}; expires=${date.toUTCString()}`;
}

function getCookie(cname) {
    let name = cname + "=";
    let decodedCookie = decodeURIComponent(document.cookie);
    let ca = decodedCookie.split(';');
    for (let i = 0; i < ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function checkCookie(cname) {
    let cookie = getCookie(cname);
    if (cookie != "") {
        return true;
    }
    return false;
}

function clearCookie() {
    document.cookie = "a_t=; expires=Thu, 01 Jan 1970 00:00:00 UTC; ";
    document.cookie = "r_t=; expires=Thu, 01 Jan 1970 00:00:00 UTC; ";
}

function logout() {
    clearCookie();
    window.location.href = "./";
}