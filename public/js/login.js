import { showLoader } from './helper.js';

let loginForm = document.getElementById('loginForm');

loginForm.onsubmit = (event) => {
    event.preventDefault();
    let username = document.getElementById('username').value;
    let password = document.getElementById('password').value;

    showLoader("btnSubmit", "18px", "18px");
    login(username, password);
}

// login with user credentials
async function login(username, password) {
    if (username != "" && password != "") {
        try {
            /* 
                make request to api for access and refresh tokens
                if successful, store tokens as cookies and redirect user to dashboard
            */
            const response = await axios.post('/login', {
                username: username,
                password: password
            }).catch((error) => {
                btnSubmit.innerHTML = "Submit";
                if (error.response.status === 401) {
                    alert(error.response.data.msg);
                }
            })

            if (response.status == 200) {
                let { access_token, refresh_token } = response.data;
                setCookie("a_t", access_token);
                setCookie("r_t", refresh_token);
                document.location.href = "./dashboard.html";
            }
        } catch (e) {
            return;
        }

    } else {
        alert("Login fields cannot be empty");
        btnSubmit.innerHTML = "Submit";
        return;
    }
}